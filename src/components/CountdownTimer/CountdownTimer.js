import React, { Component } from 'react';
import './CountdownTimer.css';

const timerCircumference = 310 // circumference at current timer's (stroke/circle) radius
const timerCircumferenceCorrection = 3
const inititalTimersState = {
  hours: 0,
  hoursStrokeOffset: timerCircumference * 2, // fill entire timer stroke
  minutes: 0,
  minutesStrokeOffset: timerCircumference * 2,
  seconds: 0,
  secondsStrokeOffset: timerCircumference * 2
}

class CountdownTimer extends Component {
  constructor() {
    super()
    this.secondsTotal = 0
    this.secondsInterval = null
    this.isTimerActive = false
    this.hoursProportionValue = 1
    this.timerInputRef = React.createRef()
    
    this.state = Object.assign({
      isHoursTimerVisible: true,
      isMinutesTimerVisible: true,
      isSecondsTimerVisible: true
    }, inititalTimersState)

    // all bindings can be removed by using transform-class-properties
    this.researchTimerValues = this.researchTimerValues.bind(this)
    this.getHoursStrokeOffset = this.getHoursStrokeOffset.bind(this)
    this.getInitMinutesStrokeOffset = this.getInitMinutesStrokeOffset.bind(this)
    this.getMinutesStrokeOffset = this.getMinutesStrokeOffset.bind(this)
    this.getSecondsStrokeOffset = this.getSecondsStrokeOffset.bind(this)

    this.onStartButtonClicked = this.onStartButtonClicked.bind(this)
    this.onResetButtonClicked = this.onResetButtonClicked.bind(this)
    this.onHoursVisibilityChange = this.onHoursVisibilityChange.bind(this)
    this.onMinutesVisibilityChange = this.onMinutesVisibilityChange.bind(this)
    this.onSecondsVisibilityChange = this.onSecondsVisibilityChange.bind(this)
  }

  researchTimerValues () {
    this.secondsInterval = setInterval(() => {
      if (this.secondsTotal <= 0) {
        clearInterval(this.secondsInterval)
        this.isTimerActive = false
      }

      this.setState((currentState) => ({
        hours: Math.floor(this.secondsTotal / 3600),
        hoursStrokeOffset: this.getHoursStrokeOffset(currentState),
        minutes: Math.floor(this.secondsTotal / 60) >= 60 ? Math.floor(this.secondsTotal / 60) - 60 * Math.floor(this.secondsTotal / 3600) : Math.floor(this.secondsTotal / 60),
        minutesStrokeOffset: this.getMinutesStrokeOffset(currentState),
        seconds: this.secondsTotal % 60,
        secondsStrokeOffset: this.getSecondsStrokeOffset(currentState)
      }), () => { // accuracy cause
        this.secondsTotal--
      })
    }, 1000)
  }

  getHoursStrokeOffset (currentState) {
    // making update only once in hour
    if (this.secondsTotal % 3600 === 0 && this.secondsTotal >= 3600) {
      return currentState.hoursStrokeOffset + this.hoursProportionValue
    }

    return currentState.hoursStrokeOffset
  }

  getInitMinutesStrokeOffset () {
    let minutesToHoursCorrectionValue = 0
    if (Math.floor(this.secondsTotal / 60) >= 60) {
      minutesToHoursCorrectionValue = 60 * Math.floor(this.secondsTotal / 3600)
    }

    return timerCircumference + timerCircumferenceCorrection + 
      (timerCircumference - ((Math.floor(this.secondsTotal / 60) - minutesToHoursCorrectionValue) / 60 * timerCircumference))
  }

  getMinutesStrokeOffset (currentState) {
    // making update only once in minute, not every second
    if (this.secondsTotal % 60 === 0 && this.secondsTotal >= 60) {
      if (Math.floor(this.secondsTotal / 60) === 60 * Math.floor(this.secondsTotal / 3600)) {
        // reset stroke offset
        return timerCircumference + timerCircumferenceCorrection
      }

      return currentState.minutesStrokeOffset + timerCircumference / 60
    }

    return currentState.minutesStrokeOffset
  }

  getSecondsStrokeOffset (currentState) {
    if (this.secondsTotal % 60 === 0 && this.secondsTotal >= 60) {
      // reset stroke offset
      return timerCircumference + timerCircumferenceCorrection
    }
    
    return currentState.secondsStrokeOffset + timerCircumference / 60
  }

  onStartButtonClicked () {
    let inputValue = parseInt(this.timerInputRef.current.value)

    if (Number.isNaN(inputValue) || inputValue < 0) {
      console.error('Provided wrong value: expected non-negative integer')
      return
    }
    if (this.isTimerActive) {
      console.error('Timer should be stopped before inputing new values')
      return
    }
    
    this.isTimerActive = true
    this.secondsTotal = inputValue
    this.hoursProportionValue = timerCircumference / (Math.floor(this.secondsTotal / 3600) + 1)

    this.setState({ // timers initialization before launching
      hoursStrokeOffset: timerCircumference + timerCircumferenceCorrection + this.hoursProportionValue,
      minutesStrokeOffset: this.getInitMinutesStrokeOffset(),
      secondsStrokeOffset: timerCircumference + (timerCircumference - ((this.secondsTotal % 60) / 60 * timerCircumference))
    },
    this.researchTimerValues)
  }

  onResetButtonClicked () {
    clearInterval(this.secondsInterval)
    this.setState(inititalTimersState)
    this.isTimerActive = false
  }

  onHoursVisibilityChange (event) {
    this.setState({ isHoursTimerVisible: event.target.checked })
  }

  onMinutesVisibilityChange (event) {
    this.setState({ isMinutesTimerVisible: event.target.checked })
  }

  onSecondsVisibilityChange (event) {
    this.setState({ isSecondsTimerVisible: event.target.checked })
  }

  render() {
    return (
      <div className="countdown-timer">
        <div className="timers-view">
          <svg width="395">
            <defs>
              <filter id="circle-shadow" x="-40%" y="-40%" width="180%" height="180%">
                <feOffset result="offOut" in="SourceAlpha" dx="1" dy="1" />
                <feGaussianBlur result="blurOut" in="offOut" stdDeviation="1" />
                <feBlend in="SourceGraphic" in2="blurOut" mode="normal" />
              </filter>
            </defs>

            {this.state.isHoursTimerVisible && <g id="hours">
              <text>
                <tspan className="value" x="16.3%" dy="50%">{this.state.hours}</tspan>
                <tspan className="title" x="16.3%" dy="10%">Hours</tspan>
              </text>

              <circle className="empty" cx="65" />
              <circle className="filled" cx="65" strokeDasharray={this.state.hoursStrokeOffset} />
            </g>}

            {this.state.isMinutesTimerVisible && <g id="minutes">
              <text>
                <tspan className="value" x="49.3%" dy="50%">{this.state.minutes}</tspan>
                <tspan className="title" x="49.3%" dy="10%">Minutes</tspan>
              </text>

              <circle className="empty" cx="195" />
              <circle className="filled" cx="195" strokeDasharray={this.state.minutesStrokeOffset} />
            </g>}

            {this.state.isSecondsTimerVisible && <g id="seconds">
              <text>
                <tspan className="value" x="82.3%" dy="50%">{this.state.seconds}</tspan>
                <tspan className="title" x="82.3%" dy="10%">Seconds</tspan>
              </text>

              <circle className="empty" cx="325" />
              <circle className="filled" cx="325" strokeDasharray={this.state.secondsStrokeOffset} />
            </g>}
          </svg>
        </div>

        <div className="timers-input">
          <div className="timers-controller">
            <button className="btn start" onClick={this.onStartButtonClicked}>Start</button>
            <input type="text" ref={this.timerInputRef} />
            <button className="btn reset" onClick={this.onResetButtonClicked}>Reset</button>
          </div>
          <div className="timers-visibility">
            Hours
            <input defaultChecked type="checkbox" onChange={this.onHoursVisibilityChange} />
            Minutes
            <input defaultChecked type="checkbox" onChange={this.onMinutesVisibilityChange} />
            Seconds
            <input defaultChecked type="checkbox" onChange={this.onSecondsVisibilityChange} />
          </div>
        </div>
      </div>
    );
  }
}

export default CountdownTimer;