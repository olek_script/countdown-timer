import React, { Component } from 'react';

import CountdownTimer from './components/CountdownTimer/CountdownTimer'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="app">
        <h3 className="title">Countdown timer app</h3>
        <CountdownTimer />
      </div>
    );
  }
}

export default App;
